var mysql = require('promise-mysql');

var connectionPool = mysql.createPool(
    {
        host: 'localhost',
        user: 'api_user',
        password: 'development_password',
        database: 'final_year_project'
    }
)

exports.connectionPool = connectionPool;
