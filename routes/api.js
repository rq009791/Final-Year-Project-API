// include libraries
var express = require('express');
var router = express.Router();
var connectionPool = require('./ConnectToMySQL').connectionPool;
var Promise = require('bluebird');
var jsonWebTokenLibrary = require('jsonwebtoken');
var bcrypt = require('bcrypt');
var fs = require('fs');

/* 20/01/2018
* http://bluebirdjs.com/docs/working-with-callbacks.html
* Used to identify ways of converting callback methods to use promises
*
*/

var cluster = Promise.promisifyAll(require('dbscan_gps'));


// check that the access token is valid

var tokenPrivateKey = fs.readFileSync('../../other/private.pem');
var tokenPublicKey = fs.readFileSync('../../other/public.pem');
/* The read rules method is one which reads in rules from the database depending on who they are to be applied to. This is designed to be called in the analytic rest calls.*/
var readRules = function (connection, tableName, actionON) {
    // get a list of rules which apply to the specified user from the database
    return connection.query('' +
        'SELECT * FROM Rule ' +
        'WHERE DataTable= ? AND Action_On = ? ' +
        '   AND Expire_Stamp IS NOT NULL ' +
        '   AND Expire_Stamp >= now()',
        [tableName, actionON]
    ).then(function (Rules) {
        // for each rule get the parameters associated with it. Each time returning a promise so that database errors can be caught in one place.
        return Promise.map(Rules, function (rule) {
            rule.Parameters = [];
            return connection.query('' +
                'SELECT * FROM Rule_Parameter ' +
                'WHERE RuleID = ?',
                [rule.RuleID]).then(function (ruleParameters) {
                // add the parameters to the current rule
                for (var counterController = 0; counterController < ruleParameters.length; counterController++) {
                    rule.Parameters.push({
                        Rule_ParameterID: ruleParameters[counterController].Rule_ParameterID,
                        Parameter_Index: ruleParameters[counterController].Parameter_Index,
                        Parameter: ruleParameters[counterController].Parameter,
                        Parameter_Type: ruleParameters[counterController].Parameter_Type,
                        RuleID: ruleParameters[counterController].RuleID
                    });
                }
                return rule;
            });
        });
    });
}


/*  10:48
    Used for application level transactions:
        https://www.codediesel.com/nodejs/mysql-transactions-in-nodejs/
        https://www.w3resource.com/node.js/nodejs-mysql.php
        https://gist.github.com/glenjamin/8924190 -- Using connection pooling

    Used for Connection pooling (efficient database use)
        https://stackoverflow.com/questions/18496540/node-js-mysql-connection-pooling
        -- make sure release() is called on transaction once it is no longer needed
*/


/*
* https://www.npmjs.com/package/jsonwebtoken
* (20/12/2017)
* used here as a reference for generating and using JSON Web tokens using an NPM library
*
*
* https://stackoverflow.com/questions/17201450/salt-and-hash-password-in-nodejs-w-crypto
* (20/12/2017)
* use bcrypt library for encryption
*
*
* https://www.npmjs.com/package/bcrypt
* (20/12/2017)
* Bcrypt functions
* */
/*This method is designed for checking that a user has been verified previously*/
var checkToken = function (req, res) {
    // get the value from the request headers
    var token = req.headers.accesstoken;
    // check that an access token has been provided
    if (!token) { // if token is undefined
        res.status(400); // bad request
        res.json({
            statusCode: 400,
            Message: 'Bad Request! AccessToken query parameter is required'
        })
        res.end();
    } else {
        // if one exists then try and decrypt it and make sure it is a valid token
        var decryptedToken;
        try {
            // the verify decrypts the token and will throw an exception if invalid
            decryptedToken = jsonWebTokenLibrary.verify(token, tokenPublicKey );
        } catch (exception) {
            res.status(401); // unauthorised
            res.json({
                statusCode: 401,
                Message: 'Unauthorised!'
            });
            res.end();
            return;
        }
        // check to make sure it has the valid fields
        // check to see if fields are present and the date is more than or equal to the current time
        if (!decryptedToken.UserID || !decryptedToken.ValidUntil ||
            decryptedToken.ValidUntil < new Date()) {
            // if not then raise an error
            res.status(401);
            res.json({
                statusCode: 401,
                Message: 'Unauthorised! Invalid access token.'
            });
            res.end();
            return;
        } else { // otherwise return the access token
            return decryptedToken;
        }
    }
}

router.post('/Login', function (req, res, next) {
    var credentials = req.body;
    var hasBeenAnInputError = false;
    if (!credentials.Username) { // if the username is not present
        hasBeenAnInputError = true;
    } else {
        if (!Number.isInteger(parseInt(credentials.Username))) {
            // if the username is not an integer then it is an input (request) error
            hasBeenAnInputError = true;
        }
    }
    // if no password has been provided then return an error
    if (!credentials.Password) {
        hasBeenAnInputError = true;
    }
    // check to see if the validation has identified an error
    if (hasBeenAnInputError) {
        res.status(400); // bad request
        res.json({
            statusCode: 400,
            Message: 'Bad Request! This method requires a Username and Password attribute.'
        });
        res.end();
    } else { // if there has been no error
        // now check to see if the user exists and retrieve their record
        var connection;
        connectionPool.getConnection().then(function (con) {
            connection = con;
            return connection.query('' +
                'SELECT * ' +
                'FROM Users ' +
                'WHERE UserID = ?',
                [credentials.Username]
            ).then(function (userResults) {
                connection.release();
                // check that the user exists
                if (userResults.length != 1) {
                    res.status(401); // unauthorised
                    res.json({
                        statusCode: 401,
                        Message: 'Unauthorised!'
                    });
                    res.end();
                } else { // otherwise the user exists
                    // now check the password
                    var databaseUser = userResults[0];
                    // compare salted hashes
                    //                      plaintext password, database hash
                    bcrypt.compare(credentials.Password, databaseUser.Password).then(function (result) {
                        if (result == true) { // if they do match then return an access token
                            var validUntil = new Date();
                            // make token valid for a month
                            validUntil.setMonth(validUntil.getMonth() + 1);
                            // first the token is generated and passed in
                            var token = jsonWebTokenLibrary.sign({
                                UserID: databaseUser.UserID,
                                ValidUntil: validUntil // used to specify a length of time which this is valid for
                            }, tokenPrivateKey, {algorithm: 'RS256'});
                            // return token
                            res.json({
                                UserID: databaseUser.UserID,
                                ValidUntil: validUntil,
                                Token: token
                            });
                            res.status(200);
                            res.end();
                        } else {
                            // if there is not a match then return an unauthorised error!
                            res.status(401);
                            res.json({
                                statusCode: 401,
                                Message: 'Unauthorised! Either the username, password or both are invalid.'
                            });
                            res.end();
                        }
                    }).catch(function (error) {
                        // if there are any errors with the salted hash comparison then return status 500
                        res.status(500);
                        res.json({
                            statusCode: 500,
                            Message: 'An unexpected server error has occurred!'
                        });
                        res.end();
                    });
                }
            });
        }).catch(function (error) {
            // catch all uncaught errors which are most likely to be database errors
            res.status(500); // server error!
            res.json({
                statusCode: 500,
                Message: 'There has been an unexpected server error!'
            });
            res.end();
        });
    }
});
/*The register method allows new users to register their account*/
router.post('/Register', function (req, res, next) {
    // get parameters from post body object
    var password = req.body.Password;
    var userType = req.body.UserType;
    // create a list of errors such that the user can get the maximum number of errors returned to them
    var errorList = [];
    if (!password) {
        // if password is not present or is empty then raise an error
        errorList.push('A password must be provided that is at least one character long.');
    } else {
	// check to see if the user passed a json object containing a password as a string
        if (typeof password != 'string') {
            errorList.push('A password must be a string value.');
        } else {
            if (password.length < 6) {
                // check to see if the password is more than 6 characters long
                errorList.push('A password must be at least 6 characters long.');
            }
        }
    }
    // check if the user type has been provided
    if (!userType) {
        errorList.push('A User type must be provided!');
    } else {
        // check the input type
        if (typeof userType != 'string') {
            errorList.push('The user type parameter must be a string.');
        } else {
            // check the value to make sure it is one of the allowed values
            if (!(userType == 'D' || userType == 'M' || userType == 'F')) {
                errorList.push("A User type must be a string which is either 'D', 'M' or 'F'.");
            }
        }
    }
    // if an error has occurred
    if (errorList.length > 0) {
        // an error has occurred
        res.status(400); // bad request
        res.json({
            staatusCode: 400,
            Message: errorList.toString()
        });
        res.end();
    } else { // otherwise continue
        // start communicating with the database
        var connection;
        connectionPool.getConnection().then(function (con) {
            connection = con;
            // hash the plaintext password
            bcrypt.hash(password, 12).then(function (hash) {
                // if the hash is successful then add the user to the database
                connection.query('' +
                    'INSERT INTO Users(Password,User_Type) ' +
                    'VALUES(?,?)',
                    [hash, userType]
                ).then(function (insertedUserRows) {
                    // the connection does not need to be maintained any more
                    connection.release();
                    var userID = insertedUserRows.insertId;
                    // return token
                    res.status(200);
                    // generate the tokens volatility date
                    var validUntil = new Date();
                    // allow the user a month before they have to log back in again
                    validUntil.setMonth(validUntil.getMonth() + 1);
                    // first the token is generated and passed in
                    var token = jsonWebTokenLibrary.sign({
                        UserID: userID,
                        ValidUntil: validUntil
                        // used to specify a length of time which this is valid for
                    }, tokenPrivateKey, {algorithm: 'RS256'});
                    res.json({
                        UserID: userID,
                        ValidUntil: validUntil,
                        Token: token
                    });
                    res.end();
                }).catch(function (error) {
                    // catch any encryption errors
                    connection.release();
                    res.status(500); // server error
                    res.json({
                        statusCode: 500,
                        Message: 'Server Error!'
                    });
                    res.end();
                });
            }).catch(function (error) {
                // catch any database errors
                res.status(500);
                res.json({
                    statusCode: 500,
                    Message: 'Error generating your access token!'
                });
                res.end();
            });
        });
    }
});

/*Utility function for checking is a submitted rule is valid. This aims to reduce the size of the rule function*/
function isRuleValid(rule) {
    // validation returning boolean stating if the rule is valid
    /// valid = true
    // invalid = false
    //https://stackoverflow.com/questions/4059147/check-if-a-variable-is-a-string-in-javascript (18/12/2017)
    // check to make sure that the required attributes are present and of the correct type
    if (!rule.Name || !(typeof rule.Name == 'string')) {
        return false;
    }
    if (!rule.Command || !(typeof rule.Command == 'string')) {
        return false;
    }
    if (!rule.ActionOn || !Number.isInteger(parseInt(rule.ActionOn))) { // the user id must be an integer
        return false;
    }
    if (!rule.DataTable || !(typeof rule.DataTable == 'string')) {
        return false;
    }
    // check that an array of parameters is present
    if (!rule.Parameters || !(rule.Parameters instanceof Array)) {
        return false;
    }
    // check to make sure that the provided command is valid
    // check the command is valid
    if (rule.Command != 'ABV' && rule.Command != 'BLO') {
        return false; // the command is not valid
    }
    // the set of tables which are valid is intended to expand in the future
    // check if the data table is valid or not
    if (rule.DataTable != 'H') {
        return false; // data table is not valid
    }
    // based on the validated command check the provided parameters
    switch (rule.Command) {
        case 'ABV':
            // check to make sure that the parameters are provided and that the expected entries are present
            if (rule.Parameters.length != 1 || rule.Parameters[0].Index == undefined ||
                rule.Parameters[0].Index != 0 || !rule.Parameters[0].Value ||
                !Number.isInteger(parseInt(rule.Parameters[0].Value)) ||
                !rule.Parameters[0].Type || typeof rule.Parameters[0].Type != 'string' ||
                rule.Parameters[0].Type != 'INT') {
                // rule requires only one parameter of type integer. It must also have an index of 0
                return false;
            }
            break;
        case 'BLO':
            // check to make sure that the parameters are provided and that the expected entries are present
            if (rule.Parameters.length != 1 || rule.Parameters[0].Index == undefined ||
                rule.Parameters[0].Index != 0 || !rule.Parameters[0].Value ||
                !Number.isInteger(parseInt(rule.Parameters[0].Value)) ||
                !rule.Parameters[0].Type || typeof rule.Parameters[0].Type != 'string' ||
                rule.Parameters[0].Type != 'INT') {
                // rule requires only one parameter of type integer. It must also have an index of 0
                return false;
            }
            break;
    }
    // if all the checks pass then everything is okay
    return true;
}

/*Method for allowing a user to create a new rule on someone*/
router.post('/New-Rule', function (req, res, next) {
    // this method is for users to add new rules for a particular user
    var token = checkToken(req, res);
    if (token) { // check a token object has been returned
        var userID = token.UserID;
        var rule = req.body;
        if (isRuleValid(rule)) { // check the rule is valid
            var applyTo = rule.ActionOn;
            // if the rule is valid then add it to the database
            // get a connection to the database
            var connection;
            connectionPool.getConnection().then(function (con) {
                connection = con;
                // otherwise continue (if there were no errors)
                // start a transaction to make sure the database does not change from one query to another
                return connection.beginTransaction().then(function () {
                    // if there has not been an error setting up the transaction then proceed
                    // https://stackoverflow.com/questions/17112852/get-the-new-record-primary-key-id-from-mysql-insert-query (14/12/2017) how to retrieve the ID from the data inserted
                    return connection.query('' +
                        'INSERT INTO Rule (Rule_Name, Rule_ActionID, Created_By, Action_On, DataTable) ' +
                        'VALUES(?,(SELECT Rule_ActionID FROM Rule_Action ' +
                        'WHERE Command = ?),?,?,?)',
                        [rule.Name, rule.Command, userID, applyTo, rule.DataTable]
                    ).then(function (rows) {
                        // if there are no errors then continue
                        var insertedID = rows.insertId;
                        // building insert query
                        var ruleParametersQuery = 'INSERT INTO Rule_Parameter(Parameter_Index, Parameter, Parameter_Type, RuleID) VALUES';
                        var parameters = rule.Parameters;
                        for (var counter = 0; counter < parameters.length; counter++) {
                            var parameter = parameters[counter];
                            if (counter > 0) {
                                ruleParametersQuery += ',';
                            }
                            // building the values to insert using escaped variables to avoid an SQL injection attack. The inserted id is not escaped because it is an integer that comes straight from the database
                            ruleParametersQuery += '(' + connection.escape(parameter.Index) + ',' +
                                connection.escape(parameter.Value) + ',' +
                                connection.escape(parameter.Type) + ',' +
                                insertedID + ')';
                        }
                        // execute the insert query
                        return connection.query(ruleParametersQuery).then(function (insertRuleParametersRows) {
                            // if there are no errors then continue
                            return connection.commit().then(function () {
                                connection.release();
                                // if there has not been an error committing then tell the user there has been a successful operation
                                res.status(200);
                                res.json({
                                    statusCode: 200,
                                    Message: 'Rule Added!'
                                });
                                res.end();
                            });
                        });
                    });
                });
            }).catch(function (error) {
                connection.rollback().then(function(){
                    res.status(500);
                    res.json({
                        statusCode: 500,
                        Message: 'Something has gone wrong while trying to for fill your request. Please try again later.'
                    });
		            res.end();
               });
            });
        } else {
            // if the rule is invalid then throw an error
            res.status(400);
            res.json({
                statusCode: 400,
                Message: 'The rule submitted is not valid. Please submit a valid rule'
            });
            res.end();
        }
    }
});
/* get a list of all the rules acting on you*/
router.get('/Rules', function (req, res, next) {
    var token = checkToken(req, res);
    if (token) {
        var userId = token.UserID;
        connectionPool.getConnection().then(function (connection) {
            connection.beginTransaction().then(function () {
                return connection.query('' +
                    'SELECT RuleID, Rule_Name, Created_By, Action_On, Authorised, Expire_Stamp ' +
                    'FROM Rule ' +
                    'WHERE Action_On = ?',
                    [userId]);
            }).then(function (selectRulesResultRows) {
                return Promise.map(selectRulesResultRows, function (element) {
                    // asynchronous
                    return connection.query('' +
                        'SELECT Rule_ParameterID, Parameter_Index, Parameter, Parameter_Type ' +
                        'FROM Rule_Parameter ' +
                        'WHERE RuleID = ? ' +
                        'ORDER BY Parameter_Index',
                        [element.RuleID]
                    ).then(function (queryResults) {
                        // add the parameters
                        element.Parameters = queryResults;
                        return element;
                    });
                });
            }).then(function (rules) {
                // once all of the rules have been read in return to the user
                connection.commit().then(function () {
                    res.status(200);
                    res.json(rules);
                    res.end();
                });
            });
        }).catch(function (error) {
            // catch all unexpected errors
            connection.rollback().then(function () {
                connection.release();
                res.json(error);
                res.status(500);
                res.end();
            });
        });
    }
});
/* Returns a list of users which have been granted authoriseations for access to your data*/
router.get('/Authorised-Users', function (req, res, next) {
    var token = checkToken(req, res);
    if (token) {
        var userID = token.UserID;
        /*https://stackoverflow.com/questions/221294/how-do-you-get-a-timestamp-in-javascript (13/12/2017)
       *
       * https://stackoverflow.com/questions/1058031/mysql-compare-two-datetime-fields
       *
       * https://stackoverflow.com/questions/23865228/how-to-store-datetime-on-server-side-in-nodejs
       *
       * https://www.w3.org/TR/NOTE-datetime
        */
        // get timestamp of the current time where the server is hosted. This should include a time zone offset
        var requestTimeStamp = new Date();
        var connection;
        connectionPool.getConnection().then(function (con) {
            connection = con;
            return connection.query('' +
                'SELECT DISTINCT(Authorised_UserID) ' +
                'FROM Authorised_Users ' +
                'WHERE Monitored_UserID = ? ' +
                'AND Authorised_Time IS NOT NULL ' +
                'AND Expiry_Time IS NOT NULL ' +
                'AND Expiry_Time >= ?',
                [userID, requestTimeStamp]
            ).then(function (rows) {
                // make sure the connection is no longer being held
                connection.release();
                res.json(rows);
                res.status(200);
                res.end();
            });
        }).catch(function (exception) {
            // catch all database errors
            connection.release();
            res.status(500);
            res.json({
                statusCode: 500,
                Message: 'An  unexpected server error has occured'
            });
            res.end();
        });
    }
});
/*Mechanism to allow user to make an access request on another one*/
router.post('/Request-Access', function (req, res, next) {
    var token = checkToken(req, res);
    if (token) {
        // check that the access token has been returned
        // get variables into a more easy to access place
        var userID = token.UserID;
        var body = req.body;
        var actionOn = body.ActionOn;
        var table = body.DataSet;
        var relation = body.Relation;
        // validating teh input
        var inputErrors = [];
        // check the required fields are present, if they are perform further validation
        if (!userID) {
            inputErrors.push('An attribute value of UserID is expected but not found.');
        } else { // check a number has been provided
            // if it is not a number then raise an error
            if (!Number.isInteger(parseInt(userID))) {
                inputErrors.push('A user id must be an integer.');
            }
        }
        if (!actionOn) {
            inputErrors.push('An attribute value of ActionOn is expected but not found.');
        } else {
            // check that an integer has been provided
            if (!Number.isInteger(parseInt(actionOn))) { // if it is not a number then raise an error
                inputErrors.push('ActionOn must be an integer.');
            }
        }
        if (!table) {
            inputErrors.push('An attribute value of DataSet is expected but not found.');
        } else {
            // check that the table is a string value
            if (typeof table != 'string') {
                // if it is not a number then raise an error
                inputErrors.push('DataSet must be a string.');
            } else {
                // check that the value corresponds to a set of predefined values
                if (!(table == 'H' || table == 'A' || table == 'G')) {
                    inputErrors.push("DataSet must be either 'H', 'A', or 'G'.");
                }
            }
        }
        if (!relation) {
            inputErrors.push('An attribute value of Relation is expected but not found.');
        } else {
            // check that relation input is a string
            if (typeof relation != 'string') { // if it is not a number then raise an error
                inputErrors.push('Relation must be a string.');
            } else {
                // check that the relation input is one of a set of predefined values
                if (!(relation == 'F' || relation == 'M' )) {
                    inputErrors.push("The relation must be either 'M' or 'F'.");
                }
            }
        }
        // check for validation errors
        if (inputErrors.length > 0) {
            res.status(400);
            res.json({
                statusCode: 400,
                Message: 'Bad Request!',
                Errors: inputErrors
            });
            res.end();
        } else {
            // if the input is valid
            var connection;
            connectionPool.getConnection().then(function (con) {
                connection = con;
                return connection.beginTransaction();
            }).then(function () {
                // check that the action on id exists in the database
                return connection.query('' +
                    'SELECT UserID ' +
                    'FROM Users ' +
                    'WHERE UserID =?',
                    [actionOn]);
            }).then(function (existingUsersQueryResult) {
                if (existingUsersQueryResult.length == 1) {
                    // does it exist in the database
                    return connection.query('' +
                        'INSERT INTO Authorised_Users(Monitored_UserID, Authorised_UserID, Relation, DataTable ) ' +
                        'VALUES(?,?,?,?)',
                        [actionOn, userID, relation, table]
                    ).then(function (insertedRowa) {
                        return connection.commit();
                    }).then(function () {
                        connection.release();
                        res.status(200);
                        res.json({
                            statusCode: 200,
                            Message: 'Request made.'
                        });
                        res.end();
                    });
                } else {
                    // if the action on user id does not exist in the database return an error
                    res.status(400);
                    res.json({
                        statusCode: 400,
                        Message: 'Bad Request!',
                        Errors: ['Specified user account does not exist.']
                    });
                    res.end();
                }
            }).catch(function (error) {
                return connection.rollback().then(function () {
                    res.status(500);
                    res.json({
                        statusCode: 500,
                        Message: 'An internal server error has occurred.'
                    });
                    res.end();
                });
            });
        }
    }
});
/*
* This method is designed for a user to approve access requests made on them
* */
router.post('/Approve-Access-Request', function (req, res, next) {
    var token = checkToken(req, res);
    if (token) { // check that the access token is valid
        // get http body variables into local variables
        var userId = token.UserID;
        var userIdToAuthorise = req.body.UserIdToAuthorise;
        var requestID = req.body.RequestID;
        // input validation
        var inputErrors = [];
        if (!userIdToAuthorise) {
            inputErrors.add("UserIdToAuthorise is required as attribute in the method body");
        } else {
            // check the data typeof the input
            if (!Number.isInteger(parseInt(userIdToAuthorise))) {
                inputErrors.add("UserIdToAuthorise should be an integer.");
            }
        }
        if (!requestID) {
            inputErrors.add("RequestID required as attribute in the method body.")
        } else {
            if (!Number.isInteger(parseInt(requestID))) {
                inputErrors.add("RequestID should be an integer.");
            }
        }
        // check to see if there have been any errors and if there has been return an error
        if (inputErrors.length > 0) {
            res.status(400);
            res.json({
                statusCode: 400,
                Message: 'Bad Request!',
                Errors: inputErrors
            });
            res.end();
        } else {
            // if the input is valid then continue
            var connection;
            connectionPool.getConnection().then(function (con) {
                connection = con;
                return connection.beginTransaction().then(function () {
                    // if there is not any error then the code will execute down here                                                                                                          here the server is getting the current UTC time to check make sure errors do not arise from a permission which is no longer valid.
                    return connection.query('' +
                        'SELECT COUNT(*) as numberOfRows ' +
                        'FROM Authorised_Users ' +
                        'WHERE Monitored_UserID = ? ' +
                        'AND Authorised_UserID = ?  ' +
                        'AND Authorised_UsersID = ?',
                        [userId, userIdToAuthorise, requestID]
                    ).then(function (rows) {
                        // if there are no errors
                        var numberOfRows = rows[0].numberOfRows;
                        // if the request does not exist then raise a request error
                        if (numberOfRows == 0) {
                            return connection.rollback().then(function () {
                                connection.release();
                                res.status(400);
                                res.json({
                                    statusCode: 400,
                                    Message: 'Bad Request!',
                                    Errors: ['The specified request to approve does not exist.']
                                });
                                res.end();
                            });
                        } else {
                            // if the amount of rows (count from the database) is 0 (count will not return less than 0 so there is not point checking it)
                            var timestamp = new Date();
                            var expireTimestamp = new Date(timestamp);
                            // one month later
                            expireTimestamp.setMonth(expireTimestamp.getMonth() + 1);
                            return connection.query('' +
                                'UPDATE Authorised_Users ' +
                                'SET ' +
                                '   Authorised_Time = ?, ' +
                                '   Expiry_Time = ? ' +
                                'WHERE Authorised_UsersID = ? ',
                                [timestamp, expireTimestamp, requestID]
                            ).then(function (insertedRows) {
                                return connection.commit().then(function () {
                                    connection.release();
                                    res.status(200);
                                    res.json({
                                        statusCode: 200,
                                        Message: 'Access request approved.'
                                    });
                                    res.end();
                                });
                            });
                        }
                    });
                });
            }).catch(function (error) {
                connection.rollback().then(function () {
                    connection.release();
                    res.status(500);
                    res.json({
                        statusCode: 500,
                        Message: 'An unexpected server error has occurred.'
                    });
                    res.end();
                });
            });
        }
    }
});
/*
 * Get all rules which require approving
 */
router.get('/Requested-Rules', function (req, res, next) {
    var token = checkToken(req, res);
    // this method returns all the rules which have pending requests
    if (token) {
        // check to see that the user has a valid access token
        var userID = token.UserID;
        var timestamp = new Date();
        var connection;
        connectionPool.getConnection().then(function (con) {
            connection = con;
            // read all of the rules in from the database which have been requested but not approved
            return connection.beginTransaction().then(function () {
                return connection.query('' +
                    'SELECT RuleID, Rule_Name, Action_Name ' +
                    'FROM Rule ' +
                    'INNER JOIN Rule_Action ON Rule.Rule_ActionID = Rule_Action.Rule_ActionID  ' +
                    'WHERE Action_On = ? ' +
                    '   AND (Authorised = FALSE ' +
                    '   OR Authorised IS NULL' +
                    '           OR Expire_Stamp < ?)',
                    [userID, timestamp]);
            }).then(function (selectRulesResultRows) {
                return Promise.map(selectRulesResultRows, function (element) {
                    // asynchronous
                    return connection.query('' +
                        'SELECT Rule_ParameterID, Parameter_Index, Parameter, Parameter_Type ' +
                        'FROM Rule_Parameter ' +
                        'WHERE RuleID = ? ' +
                        'ORDER BY Parameter_Index',
                        [element.RuleID]
                    ).then(function (queryResults) {
                        // add the parameters
                        element.Parameters = queryResults;
                        return element;
                    });
                });
            }).then(function (rules) {
                // if the rules have been successfully retrieved then report success
                return connection.commit().then(function () {
                    res.status(200);
                    res.json(rules); // return the rules
                    res.end();
                });
            });
        }).catch(function (error) {
            // catch all unexpected problems such as database errors
            connection.rollback().then(function () {
                connection.release();
                res.json({
                    ststusCode: 500,
                    Message: 'An unexpected server error occurred.'
                });
                res.status(500);
                res.end();
            });
        });
    }
});
/* Method to approve a rule which has been requested on you*/
router.post('/Approve-Rule/:RuleID', function (req, res, next) {
    var token = checkToken(req, res);
    if (token) { // check the access token is valid
        var userID = token.UserID;
        // get the rule id section of the URL
        var ruleID = req.params.RuleID;
        // validation
        var errorList = [];
        if (!ruleID) {
            //if rule id is not present then log error
            errorList.add('RuleID must be provided in the URL.');
        } else {
            if (!Number.isInteger(parseInt(ruleID))) {
                errorList.add('RuleID in url must be an integer');
            }
        }
        // check for any errors
        if (errorList.length > 0) {
            res.status(400);
            res.json({
                statusCode: 400,
                Message: 'Bad Request!',
                Errors: errorList
            });
        } else { // if there have been no errors then continue
            connectionPool.getConnection().then(function (connection) {
                // if an error has not occurred then do this
                return connection.beginTransaction().then(function () {
                    // checking the rule exists and is assigned to the user
                    return connection.query('' +
                        'SELECT * ' +
                        'FROM Rule ' +
                        'WHERE RuleID = ? ' +
                        'AND Action_On = ?',
                        [ruleID, userID]);
                }).then(function (rows) {
                    if (rows.length == 1) {
                        // does the rule exist and is it assigned to the user
                        // there should be no more than 1 as the query uses the primary key
                        var timestamp = new Date();
                        var ruleExpiryTime = new Date(timestamp);
                        ruleExpiryTime.setMonth(ruleExpiryTime.getMonth() + 1);
                        return connection.query('' +
                            'UPDATE Rule ' +
                            'SET Expire_Stamp = ?, ' +
                            '   Authorised = TRUE ' +
                            'WHERE RuleID=? ' +
                            '   AND Action_On = ?',
                            [ruleExpiryTime, ruleID, userID]
                        ).then(function (updateRows) {
                            // here rows will only return the number of rows affected. However since this is wrapped in a transaction the rule will not go anywhere.
                            // if there is not an error updating the data then continue
                            return connection.commit().then(function () {
                                // if there has not been an error committing then return successful
                                connection.release();
                                res.status(200);
                                res.json({
                                    statusCode: 200,
                                    Message: 'Rule Approved.'
                                });
                                res.end();
                                // successful end
                            });
                        })
                    } else { /// if the rule does not exist
                        return connection.rollback(function () {
                            connection.release();
                            res.status(400);
                            res.json({
                                statusCode: 400,
                                Message: "The rule does not exist or is not assigned to you."
                            });
                            res.end();
                        });
                    }
                });
            }).catch(function (error) { // catch all errors
                res.json({
                    statusCode: 500,
                    Message: 'An unexpected server error has occurred.'
                });
                res.status(500);
                res.end();
            });
        }
    }
});


// heart rate information
router.get('/Heart-Rate-Information/:UserID', function (req, res, next) {
    // this method expects the query parameters of a start time and end time
    var token = checkToken(req, res);
    if (token) {
        var userID = token.UserID;
        // https://webapplog.com/url-parameters-and-routing-in-express-js/ (13/12/2017)
        var actionOn = req.params.UserID;
        var startFrom = req.query.Start_Time;
        var endAt = req.query.End_Time;
        /*
        * Error handling in express 13/12/2017
        *
        * Use next over throwing errors
        *
        * https://stackoverflow.com/questions/27794750/node-js-with-express-throw-error-vs-nexterror
        *http://expressjs.com/en/guide/error-handling.html
        * */
        // validation
        var errorList = [];
        if (!startFrom) {
            errorList.push("API method requires a 'Start_Time' parameter");
            // if undefined then raise error
        } else {
            if (isNaN(Date.parse(startFrom)) ) {
                errorList.push('Start_Time is not a valid time.');
            }
        }
        if (!endAt) {
            errorList.push("API method requires a 'End_Time' parameter");
            // if undefined then raise error
        } else {
            if (isNaN(Date.parse(endAt))) {
                errorList.push('End_Time is not a valid time.');
            }
        }
        if (!actionOn) {
            errorList.push("API method requires a user id to act on in the URL.");
            // if undefined then raise error
        } else {
            if (!(parseInt(actionOn))) {
                errorList.push('The user id provided in the URL must be an integer.');
            }
        }
        // check for validation errors
        if (errorList.length > 0) {
            res.status(400);
            res.json({
                statusCode: 400,
                Message: 'Bad Request!',
                Errors: errorList
            });
            res.end();
        } else {
            // if there were no input errors then continue
            var connection;
            connectionPool.getConnection().then(function (con) {
                connection = con;
                return connection.beginTransaction();
            }).then(function () {
                // check if the user is authorised to access this data
                return connection.query("" +
                    "SELECT COUNT(*) AS AuthorisedUsersCount " +
                    "FROM Authorised_Users " +
                    "WHERE Authorised_Time IS NOT NULL " +
                    "   AND Authorised_UserID = ? " +
                    "   AND Monitored_UserID = ? " +
                    "   AND Expiry_Time >= ? " +
                    "   AND DataTable = 'H' ",
                    [userID, actionOn, new Date()]);
            }).then(function (authorisedRows) {
                if (authorisedRows[0].AuthorisedUsersCount > 0 || userID == actionOn) {
                    // in this case the user is authorised
                    return connection.query('' +
                        'SELECT Reading, Stamp ' +
                        'FROM Heart_Rate_Reading ' +
                        'WHERE UserID = ? ' +
                        '   AND Stamp >= ? ' +
                        '   AND Stamp <= ? ' +
                        'ORDER BY Stamp',
                        [actionOn, startFrom, endAt]
                    ).then(function (data) {
                        // return the data
                        return connection.commit().then(function () {
                            connection.release();
                            res.json(data);
                            res.status(200);
                            res.end();
                        });
                    });
                } else {
                    // not unauthorised
                    return connection.rollback().then(function () {
                        connection.release();
                        res.status(401);
                        res.send({
                            statusCode: 401,
                            Message: 'Unauthorised!'
                        });
                        res.end();
                    });
                }
            }).catch(function (error) {
                // catch all the errors that have been encountered
                return connection.rollback(function () {
                    connection.release();
                    res.status(500);
                    res.json({
                        statusCode: 500,
                        Message: 'An unexpected server error has occurred.'
                    });
                    res.end();
                })
            });
        }
    }
});


// accelerometer
router.get('/Accelerometer-Information/:UserID', function (req, res, next) {
    // this method expects the query parameters of a start time and end time
    // the last query parameter has been corrected (end-date) as it was inaccurate
    var token = checkToken(req, res);
    if (token) {
        // check to see if a valid token has been encountered
        var userID = token.UserID;
        // get inputs from the URL
        var actionOn = req.params.UserID;
        // get the relevant query parameters
        var startFrom = req.query.Start_Time;
        var endAt = req.query.End_Time;
        var errorList = [];
        if (!startFrom) {
            errorList.push("API method requires a 'Start_Time' parameter");
            // if undefined then raise error
        } else {
            if (isNaN(Date.parse(startFrom))) {
                errorList.push('Start_Time is not a valid time.');
            }
        }
        if (!endAt) {
            errorList.push("API method requires a 'End_Time' parameter");
            // if undefined then raise error
        } else {
            if (isNaN(Date.parse(endAt))) {
                errorList.push('End_Time is not a valid time.');
            }
        }
        if (!actionOn) {
            errorList.push("The requested URL requires an ActionOn user ID parameter");
            // if undefined then raise error
        } else {
            if (!Number.isInteger(parseInt(actionOn))) {
                errorList.push('The user id provided in the url must be an integer');
            }
        }
        // check to see if there have been any validation errors
        if (errorList.length > 0) {
            res.status(400);
            res.json({
                statusCode: 400,
                Message: 'Bad Request!',
                Errors: errorList
            });
            res.end();
        } else {
            // if there were no input errors
            var connection;
            connectionPool.getConnection().then(function (con) {
                connection = con;
                return connection.beginTransaction();
            }).then(function () {
                // check to see if the user has permission to see this information
                return connection.query("" +
                    "SELECT COUNT(*) AS AuthorisedUsersCount " +
                    "FROM Authorised_Users " +
                    "WHERE Authorised_Time IS NOT NULL " +
                    "   AND Authorised_UserID = ? " +
                    "   AND Monitored_UserID = ? " +
                    "   AND Expiry_Time >= ? " +
                    "   AND DataTable = 'A'",
                    [userID, actionOn, new Date()]);
            }).then(function (authorisationQuery) {
                // CHECK THE ACCESS RIGHTS OF THE INDIVIDUAL
                if (authorisationQuery[0].AuthorisedUsersCount > 0 || userID == actionOn) {
                    // the user is authorised to access this information
                    // if authorised then return the data requested
                    return connection.query('' +
                        'SELECT Reading, Stamp ' +
                        'FROM Acceleromiter_Reading ' +
                        'WHERE UserID = ? ' +
                        '   AND Stamp >= ? ' +
                        '   AND Stamp <= ? ' +
                        'ORDER BY Stamp',
                        [actionOn, startFrom, endAt]
                    ).then(function (rows) { // if everything is ok
                        // return the data
                        connection.commit().then(function () {
                            connection.release();
                            res.json(rows);
                            res.status(200);
                            res.end();
                        });
                    });
                } else {
                    // if not authorised then rollback
                    connection.rollback().then(function () {
                        connection.release();
                        res.status(401);
                        res.send({
                            statusCode: 401,
                            Message: 'Unauthorised!'
                        });
                        res.end();
                    });
                }
            }).catch(function (error) {
                // catch all errors
                connection.rollback().then(function () {
                    connection.release();
                    res.status(500);
                    res.json({
                        statusCode: 500,
                        Message: 'An unexpected server error has occurred.'
                    });
                    res.end();
                })
            });
        }
    }
});

/*List the user id's of the people you are monitoring*/
router.get('/Monitored-Users', function (req, res, next) {
    var token = checkToken(req, res);
    if (token) {
        // check a valid token has been provided
        var userID = token.UserID;
        var relation = req.query.Relation;
        //validation
        var errorList = [];
        if (!relation) {
            // check the parameter exists
            errorList.push('A Relation query parameter was expected but not found.');
        } else {
            // check the type and then the value of the parameter
            if (typeof relation != 'string') {
                errorList.push('The provided Relation query parameter should be a string.');
            } else {
                if (!(relation == 'F' || relation == 'M')) {
                    errorList.push('The relation value must be either F or M.');
                }
            }
        }
        // check to see if there have been any errors
        if (errorList.length > 0) {
            res.status(400);
            res.json({
                statusCode: 400,
                Message: 'Bad Request!',
                Errors: errorList
            });
            res.end();
        } else { // if there has not been any invalid entries then get the data
            var connection;
            connectionPool.getConnection().then(function (con) { //                                                                                                                     get the current date on the server
                connection = con;
                return connection.query('' +
                    'SELECT DISTINCT(Monitored_UserID) ' +
                    'FROM Authorised_Users ' +
                    'WHERE Authorised_UserID = ? ' +
                    '   AND Expiry_Time IS NOT NULL ' +
                    '   AND Expiry_Time >= ? ' +
                    '   AND Relation = ?',
                    [userID, new Date(), relation]
                ).then(function (rows) {
                    // assuming there is no error
                    res.json(rows);
                    connection.release();
                    res.status(200);
                    res.end();
                });
            }).catch(function (error) {
                // catch all errors
                connection.release();
                res.status(500);
                res.json({
                    statusCode: 500,
                    Message: 'An unexpected server error has occurred.'
                });
                res.end();
            });
        }
    }
});


/*Method for adding heart rate readings*/
router.post('/Add-Heart-Rate-Records', function (req, res, next) {
    var token = checkToken(req, res);
    if (token) {
        var userID = token.UserID;
        //validation
        var errorList = [];
        var arrayOfEntries = req.body;
        // https://stackoverflow.com/questions/27773418/check-if-express-request-data-is-array (15/12/2017)
        if (!arrayOfEntries instanceof Array) {
            // if not an array then throw an error;
            errorList.add("Input must be an array of elements.");
        } // check to see if there have been any errors so far
        if (errorList.length > 0) {
            res.status(400);
            res.json({
                statusCode: 400,
                Message: 'Bad Request!',
                Errors: errorList
            });
            res.end();
        } else {
            // if there has not been any error so far then continue
            // further validation and sql construction
            var inputErrors = [];
            var sqlInsertStatement = 'INSERT INTO Heart_Rate_Reading(Stamp,Reading,UserID, Analysed) VALUES ';
            // loop over all of the input records
            var sqlBindVariablesList = [];
            for (var counter = 0; counter < arrayOfEntries.length; counter++) {
                // get the current iterations value
                var dataItem = arrayOfEntries[counter];
                if (!dataItem.TimeStamp) {
                    // if attribute is not present then raise an error
                    //https://stackoverflow.com/questions/19084570/how-to-add-items-to-array-in-nodejs (15/12/2017) -> adding elements to list;
                    inputErrors.push('TimeStamp attribute is missing.');
                } else {
                    if (isNaN(Date.parse(dataItem.TimeStamp))) {
                        // if the input is not a date then raise an error
                        inputErrors.push('TimeStamp attribute is not a valid date');
                    }
                }
                if (typeof dataItem.Reading == 'undefined' || dataItem.Reading == null) {
                    // if attribute is not present then raise an error
                    //https://stackoverflow.com/questions/19084570/how-to-add-items-to-array-in-nodejs (15/12/2017) -> adding elements to list;
                    inputErrors.push('Reading attribute is missing.');
                } else {
                    if (!Number.isInteger(parseInt(dataItem.Reading))) {
                        inputErrors.push('Reading attribute should be an integer.');
                    }
                }
                if (counter > 0) {
                    sqlInsertStatement += ',';
                }
                sqlBindVariablesList.push(dataItem.TimeStamp);
                sqlBindVariablesList.push(dataItem.Reading);
                sqlBindVariablesList.push(userID);
                sqlInsertStatement += '(?,?,?,FALSE)'
            }
            // check for any input errors
            if (inputErrors.length > 0) { // if there is anything in this list that there have been errors.
                res.status(400);
                res.json({
                    statusCode: 400,
                    Message: 'Bad Request!',
                    Errors: inputErrors
                });
            } else { // otherwise assuming there are no input errors then continue
                var connection;
                connectionPool.getConnection().then(function (con) {
                    connection = con;
                    // if there were no errors then continue
                    // insert data into the database
                    return connection.query(sqlInsertStatement, sqlBindVariablesList
                    ).then(function (rows) {
                        // if there has been no errors then continue
                        connection.release();
                        res.status(200);
                        res.json({
                            statusCode: 200,
                            Message: 'Results Inserted!'
                        });
                        res.end();
                    });
                }).catch(function (error) {
                    connection.release();
                    res.status(500);
                    res.json({
                        statusCode: 500,
                        Message: 'An unexpected server error has occurred'
                    });
                    res.end();
                });
            }
        }
    }
});


router.post('/Add-Accelerometer-Records', function (req, res, next) {
    var token = checkToken(req, res);
    if (token) {
        var userID = token.UserID;
        // validation
        var errorList = [];
        var arrayOfEntries = req.body;
        // https://stackoverflow.com/questions/27773418/check-if-express-request-data-is-array (15/12/2017)
        if (!arrayOfEntries instanceof Array) { // if not an array then raise an error;
            errorList.push("Input must be an array of elements.");
        }
        // check to see if any errors have been encountered so far
        if (errorList.length > 0) {
            res.status(400);
            res.json({
                statusCode: 400,
                Message: 'Bad Request!',
                Errors: errorList
            });
            res.end();
        } else {
            var sqlInsertStatement = 'INSERT INTO Acceleromiter_Reading(Stamp,Reading,UserID, Analysed) VALUES ';
            var inputErrors = [];
            var sqlBindVariables = [];
            // loop over all the entries being submitted and validate the. at the same time constructing the SQL query
            for (var counter = 0; counter < arrayOfEntries.length; counter++) {
                // further validate
                var dataItem = arrayOfEntries[counter];
                if (!dataItem.TimeStamp) {
                    // if attribute is not present then raise an error
                    //https://stackoverflow.com/questions/19084570/how-to-add-items-to-array-in-nodejs (15/12/2017) -> adding elements to list;
                    inputErrors.push('TimeStamp attribute is missing.');
                } else {
                    if (isNaN(Date.parse(dataItem.TimeStamp))) {
                        inputErrors.push('TimeStamp attribute is not a valid date.');
                    }
                }
                if (typeof dataItem.Reading == 'undefined' || dataItem.Reading == null) {
                    // if attribute is not present then raise an error
                    //https://stackoverflow.com/questions/19084570/how-to-add-items-to-array-in-nodejs (15/12/2017) -> adding elements to list;
                    inputErrors.push('Reading attribute is missing.');
                } else { // if it is not a number
                    if (isNaN(parseFloat(dataItem.Reading))) {
                        inputErrors.push('Reading attribute is not a number.');
                    }
                }
                // after the first element every other element needs a comma prefixing it in the sql
                if (counter > 0) {
                    sqlInsertStatement += ',';
                }
                // construct SQL
                sqlBindVariables.push(dataItem.TimeStamp);
                sqlBindVariables.push(dataItem.Reading);
                sqlBindVariables.push(userID);
                sqlInsertStatement += '(?,?,?,FALSE)'
            }
            // check to see if there has been any input errors
            if (inputErrors.length > 0) {
                // if there is anything in this list that there have been errors.
                res.status(400);
                res.json({
                    statusCode: 400,
                    Message: 'Bad Request!',
                    Errors: inputErrors
                });
                res.end();
            } else {
                // if there have been no input errors
                var connection;
                connectionPool.getConnection().then(function (con) {
                    connection = con;
                    // execute SQL
                    return connection.query(sqlInsertStatement, sqlBindVariables
                    ).then(function (rows) {
                        // if there has been no errors then continue
                        connection.release();
                        res.status(200);
                        res.json({
                            statusCode: 200,
                            Message: 'Records Added'
                        });
                        res.end();
                    });

                }).catch(function (error) {
                    // catch all errors
                    connection.release();
                    res.status(500);
                    res.json({
                        statusCode: 500,
                        Message: 'An unexpected server error has occurred.'
                    });
                    res.end();
                });
            }
        }
    }
});
/*
* Reference Jules for:
*
* use of .foreach(function(){}).then(function(){//do something});
* in order to make sure everything in the loop has been done before moving on. This may need to npm install bluebird
* */
router.post('/Add-GPS-Records', function (req, res, next) {
    var token = checkToken(req, res);
    if (token) {
        var userID = token.UserID;
        var arrayOfEntries = req.body;
        // https://stackoverflow.com/questions/27773418/check-if-express-request-data-is-array (15/12/2017)
        if (!arrayOfEntries instanceof Array) {
            // if not an array then throw an error;
            res.status(400);
            res.json({
                statusCode: 400,
                Message: 'Bad Request',
                Errors: ['This method requires an array input']
            });
            res.end();
        } else {
            // looping over the array and validating input
            // checked later
            var inputErrors = [];
            for (var counter = 0; counter < arrayOfEntries.length; counter++) {
                // get the object at the current iteration
                var dataItem = arrayOfEntries[counter];
                if (!dataItem.TimeStamp) {
                    inputErrors.push('A TimeStamp parameter myst e provided with every entry to this method.');
                }
                if (isNaN(Date.parse(dataItem.TimeStamp))) {
                    // if it is not a data then report it
                    inputErrors.push('The TimeStamp attribute but be a Date/Time in the UTC format');
                }
                if (typeof dataItem.Longitude == 'undefined' || dataItem.Longitude == null) {
                    inputErrors.push('A Longitude parameter must be provided with every entry to this method.');
                } else {
                    if (isNaN(parseFloat(dataItem.Longitude))) {
                        inputErrors.push('A Longitude parameter must be a number.');
                    } else {
                        if (dataItem.Longitude > 180 || dataItem.Longitude < -180) {
                            inputErrors.push('A Longitude parameter must be between -180 and 180.');
                        }
                    }
                }
                if (typeof dataItem.Latitude == 'undefined' || dataItem.Latitude == null) {
                    inputErrors.push('A Latitude parameter must be provided with every entry to this method.');
                } else {
                    if (isNaN(parseFloat(dataItem.Latitude))) {
                        inputErrors.push('A Latitude parameter must be a number.');
                    } else {
                        if (dataItem.Latitude > 90 || dataItem.Latitude < -90) {
                            inputErrors.push('A Latitude parameter must be between -90 and 90.');
                        }
                    }
                }
            }
            // check to see if there have been any errors
            if (inputErrors.length > 0) {
                // return an input error to the caller
                res.status(400);
                res.json({
                    statusCode: 400,
                    Message: 'Bad Request!',
                    Errors: inputErrors
                });
                res.end();
            } else { // start adding the data to the database
                var connection;
                connectionPool.getConnection().then(function (con) {
                    connection = con;
                    // if there were no errors then continue
                    return connection.beginTransaction();
                }).then(function () {
                    // if there are no errors
                    // for each element in the input array add the record to the database
                    return Promise.map(arrayOfEntries, function (dataItem) {
                        // insert the coordinate
                        return connection.query('INSERT INTO Coordinate(Longitude, Latitude) VALUES (?,?)',
                            [dataItem.Longitude, dataItem.Latitude]
                        ).then(function (coordinateInsertResults) {
                            // using the inserted id add a reading
                            var insertedID = coordinateInsertResults.insertId;
                            return connection.query('' +
                                'INSERT INTO GPS_Readings(Stamp, UserID, CoordinateID, Analysed) ' +
                                'VALUES(?,?,?,FALSE)',
                                [dataItem.TimeStamp, userID, insertedID]);
                        });
                    }).then(function () {
                        // after everything has been inserted commit the changes
                        return connection.commit().then(function () {
                            // there has been a successful set of database operations so commit
                            connection.release();
                            res.status(200);
                            res.json({
                                statusCode: 200,
                                Message: 'Records added successfuly'
                            });
                            res.end();
                        });
                    });
                }).catch(function (exception) {
                    // catch all errors such as database operation errors
                    connection.rollback().then(function () {
                        connection.release();
                        res.status(500); // raising a server error to the caller
                        res.json({
                            statusCode: 500,
                            Message: 'An internal server error has occurred.'
                        });
                        res.end();
                    });
                });
            }
        }
    }
});


router.get('/Analyse-Heart-Rate/:UserID', function (req, res, next) {
    var token = checkToken(req, res);
    if (token) {
        // check that the user is authorised
        var userID = token.UserID;
        var actionOn = req.params.UserID;
        // no need to check to see if the user id is provided as it makes part of the routing information
        if (!Number.isInteger(parseInt(actionOn))) {
            // if the query parameter is not an integer then return a bad request error message
            // return an input error to the user
            res.status(400);
            res.json({
                statusCode: 400,
                Message: 'The userID provided in the url is not a whole number.'
            });
            res.end();
        } else { // if successful
            // get a connection
            var connection;
            // the following variable is used later but needs more global scope so that it can be used in several places.
            var oneMonthAgo = new Date(); // GET TODAYS DATE
            oneMonthAgo.setMonth(oneMonthAgo.getMonth() - 1); // one month previously
            var medianHeartBeat; // in beats per minute
            var medianMovement; // accelerometer information
            // start interacting with the database
            connectionPool.getConnection().then(function (con) {
                connection = con;
                return connection.beginTransaction();
            }).then(function () {
                // the transaction has been started
                //query authorised users to make sure that the user can call this function on the specified user
                return connection.query("SELECT COUNT(*) as numberOfRows " +
                    "                   FROM Authorised_Users " +
                    "                   WHERE Monitored_UserID = ? " +
                    "                   AND Authorised_UserID = ? " +
                    "                   AND Expiry_Time >= ? " +
                    "                   AND DataTable = 'H'",
                    [actionOn, userID, new Date()]);
            }).then(function (rows) {
                // inspect the number of rows returned
                if (rows.numberOfRows == 0 && userID != actionOn) {
                    // check to see if the user is able to access this data
                    res.status(401);
                    res.json({
                        statusCoe: 401,
                        Message: 'Unauthorised to make this request on this user'
                    });
                    res.end();
                } else {
                    // select all of the readings for the monitored user id between now and one month ago
                    return connection.query('SELECT Reading ' +
                        '                   FROM Heart_Rate_Reading ' +
                        '                   WHERE UserID = ? AND ' +
                        '                   (Analysed = TRUE AND Stamp >= ?) ' +
                        '                   ORDER BY Reading',
                        [actionOn, oneMonthAgo]).then(function (medianResult) {
                        // calculating median
                        if (medianResult.length == 0) {
                            medianHeartBeat = 0;
                        } else if (medianResult.length % 2 == 0) { // if it is an even number
                            var dividedByTwo = medianResult.length / 2;
                            // get the middle two elements and divide by 2
                            medianHeartBeat = (medianResult[dividedByTwo].Reading +
                                medianResult[dividedByTwo - 1].Reading) / 2;
                        } else { // odd number
                            // select the middle element iin the list
                            medianHeartBeat = medianResult[(medianResult.length - 1) / 2].Reading;
                        }
                        // select all of the acceleration data between the same time period
                        return connection.query('' +
                            'SELECT Reading ' +
                            'FROM Acceleromiter_Reading ' +
                            'WHERE UserID = ? AND ' +
                            '   (Analysed = TRUE AND Stamp >= ?) ' +
                            'ORDER BY Reading',
                            [actionOn, oneMonthAgo]);
                        // do some query in both cases
                    }).then(function (medianAccelerationResults) {  // calculate the median acceleration
                        if (medianAccelerationResults.length == 0) {
                            medianMovement = 0;
                        }
                        else if (medianAccelerationResults.length % 2 == 0) { // if it is even
                            var dividedByTwo = medianAccelerationResults.length / 2;
                            // average of middle two results results
                            medianMovement = (medianAccelerationResults[dividedByTwo].Reading +
                                medianAccelerationResults[dividedByTwo - 1].Reading) / 2;
                        } else { // if it is odd
                            // get the middle element in the list
                            medianMovement = medianAccelerationResults[(medianAccelerationResults.length - 1) / 2].Reading;
                        }
                        // then run another query returning a promise
                        // select all the un-analysed data
                        return connection.query('' +
                            'SELECT Heart_Rate_ReadingID, Reading, Stamp ' +
                            'FROM Heart_Rate_Reading ' +
                            'WHERE UserID = ? ' +
                            'AND Analysed = FALSE ' +
                            'ORDER BY Stamp',
                            [actionOn]);
                    }).then(function (heartRateReadings) {
                        // for each heart rate reading
                        return Promise.map(heartRateReadings, function (reading) {
                            // check to see if the value is to high
                            if (reading.Reading > medianHeartBeat) {
                                // if it is two high
                                // check to see if the previous few minutes of movement are above average
                                var aFewMinutesAgo = new Date(reading.Stamp);
                                aFewMinutesAgo.setMinutes(aFewMinutesAgo.getMinutes() - 10);
                                // get the last few minutes of acceleration information
                                return connection.query('SELECT Reading ' +
                                    '                   FROM Acceleromiter_Reading ' +
                                    '                   WHERE UserID = ? ' +
                                    '                   AND Stamp >= ? ' +
                                    '                   AND Stamp <=? ' +
                                    '                   ORDER BY Stamp',
                                    [actionOn, aFewMinutesAgo, reading.Stamp]
                                ).then(function (localMovementData) {
                                    // on the data which is a minute before work out the average
                                    // calculate the local average of accelerometer readings
                                    var total = 0;
                                    for (var counter = 0; counter < localMovementData.length; counter++) {
                                        total += localMovementData[counter].Reading;
                                    }
                                    if (!((total / localMovementData.length) > medianMovement)) {
                                        // if acceleration average is not more on average then there is a problem so raise a notification
                                        return addWarning(connection, 'Heart rate higher than average',
                                            'The heart rate is above the recent statistical average'
                                            + reading.Stamp, 'H', actionOn);
                                    } else { // run a query in order to return a promise.
                                        // THIS IS A RANDOM QUERY SO THAT A PROMISE IS RETURNED
                                        return connection.query('SELECT NOW()');
                                    }
                                });
                            } else if (reading.Reading < medianHeartBeat) {
                                // if the heart rate is below the median value
                                // if it is too low
                                //check to if the previous few minutes of movement are below average
                                var aFewMinutesAgo = new Date(reading.Stamp);
                                // create a time stamp 10 minutes before the one currently being analysed
                                aFewMinutesAgo.setMinutes(aFewMinutesAgo.getMinutes() - 10);
                                return connection.query('SELECT Reading ' +
                                    '                    FROM Acceleromiter_Reading ' +
                                    '                    WHERE UserID = ? ' +
                                    '                    AND Stamp >= ? ' +
                                    '                    AND Stamp <=? ' +
                                    '                    ORDER BY Stamp',
                                    [actionOn, aFewMinutesAgo, reading.Stamp]
                                ).then(function (localMovementData) {
                                    // on the data which is a minute before work out the average
                                    // calculate the local average (Mode) of accelerometer readings
                                    var total = 0;
                                    for (var counter = 0; counter < localMovementData.length; counter++) {
                                        total += localMovementData[counter].Reading;
                                    }
                                    if (!((total / localMovementData.length) < medianMovement)) {
                                        return addWarning(connection, 'Heart rate lower than average',
                                            'The heart rate is below the recent statistical average at '
                                            + reading.Stamp, 'H', actionOn);
                                    } else {
                                        return connection.query('SELECT NOW()');
                                        // THIS IS A RANDOM QUERY SO THAT A PROMISE IS RETURNED
                                    }
                                });
                            } else {
                                // the value is equal to the median
                                // THIS IS A RANDOM QUERY SO THAT A PROMISE IS RETURNED
                                return connection.query('SELECT NOW()');
                            }
                        }).then(function (dataRows) {
                            // apply the custom rules
                            // read the rules from the database which are to do with the heart rate table that act on the specified individual
                            // get all the rules for a user to do with heart rate
                            return readRules(connection, 'H', actionOn);
                        }).then(function (rules) {
                            // for each heart rate data item
                            return Promise.map(heartRateReadings, function (dataItem) {
                                // for each rule apply the rule
                                return Promise.map(rules, function (rule) {
                                    // do different behaviour depending on the action
                                    switch (rule.Rule_ActionID) {
                                        case 5: // above command
                                            // check to see if the reading is above a set maximum
                                            if (dataItem.Reading > rule.Parameters[0].Parameter) {
                                                // there should always be a rule parameter of 0 as it is validated on the input
                                                // add a new notification
                                                return addWarning(connection, rule.Rule_Name,
                                                    'The Rule has triggered an event because the value is above the limit defined. The value encountered was '
                                                    + dataItem.Reading + ' on ' + dataItem.Stamp + '.', 'H', actionOn);
                                            }
                                            break;
                                        case 6: // below command
                                            // check to see if the value is below a set minimum
                                            if (dataItem.Reading < rule.Parameters[0].Parameter) {
                                                // there should always be a rule parameter of 0 as it is validated on the input
                                                // add a new notification
                                                return addWarning(connection, rule.Rule_Name,
                                                    'The Rule has triggered an event because the value is below the limit defined. The value encountered was '
                                                    + dataItem.Reading + ' on ' + dataItem.Stamp + '.', 'H', actionOn);
                                            }
                                            break;
                                    }
                                });
                            });
                        });
                    }).then(function (dataRows) {
                        // after all of the rules have been applied update the rows such that they have now been analysed
                        return connection.query('UPDATE Heart_Rate_Reading SET ' +
                            '                    Analysed = TRUE ' +
                            '                    WHERE UserID = ?',
                            [actionOn]);
                        // make sure all the readings are now analysed
                    }).then(function (updateRows) {
                        // set all of the accelerometer information to have been analysed.
                        return connection.query('UPDATE Acceleromiter_Reading ' +
                            '                    SET Analysed = TRUE ' +
                            '                    WHERE UserID = ?',
                            [actionOn]);
                        // make sure all the readings are now all analysed
                    }).then(function (dataRows) {
                        // everything has been successful
                        return connection.commit();
                    }).then(function () {
                        connection.release();
                        res.status(200);
                        res.json({
                            statusCode: 200,
                            Message: 'Successful analysis!'
                        });
                        res.end();
                    });
                }
            }).catch(function (error) {
                // upon any unexpected error return error to the user.
                res.status(500);
                res.json({
                    statusCode: 500,
                    Message: 'A server error has occurred while trying to complete the request.'
                });
                res.end();
            });
        }
    }
});

// a function for reading in gps data which has not been analysed
var readGpsReadings = function (connection, userID) {
    return connection.query('SELECT g.GPS_ReadingsID, g.Stamp, g.UserID,  c.Latitude, c.Longitude ,g.Analysed ' +
        '                   FROM GPS_Readings g ' +
        '                   INNER JOIN Coordinate c ON g.CoordinateID = c.CoordinateID ' +
        '                   WHERE UserID = ? AND Analysed = FALSE ORDER BY Stamp',
        [userID]);
};

// a method for returning a journey which matches the one provided
var getExistingMatchingJourneys = function (connection, userID, journey) {
    // this should only return one or 0 rows if the rest of the system is implemented properly
    return connection.query('SELECT GPS_EndpointsID ' +
        '                   FROM GPS_Endpoints ' +
        '                   WHERE UserID = ? ' +
        '                       AND (SELECT COUNT(*) ' +
        '                   FROM Coordinate ' +
        '                   WHERE CoordinateID = Point_A ' +
        '                       AND Longitude = ? AND Latitude = ? ) > 0 ' +
        '                       AND (SELECT COUNT(*) ' +
        '                       FROM Coordinate ' +
        '                       WHERE CoordinateID = Point_B ' +
        '                           AND Longitude = ? AND Latitude = ? ) > 0',
        [userID, journey.PointA.Longitude,
            journey.PointA.Latitude,
            journey.PointB.Longitude,
            journey.PointB.Latitude]);
    // the above is a horrendously long query including sub queries
};

// the analyse gps data function notes any times where the monitored individual makes a journey and then analyses this pattern for abnormal behaviour
router.get('/Analyse-GPS/:UserID', function (req, res, next) {
    var token = checkToken(req, res);
    if (token) { // check that the user has provided a valid token
        var userID = token.UserID;
        // retrive the userId from the URL
        var actionOn = req.params.UserID;
        // check that the user id is a valid type
        if (!Number.isInteger(parseInt(actionOn))) {
            // if it is not an integer then raise an input error
            res.status(400);
            res.json({
                statusCode: 400,
                Message: 'The urls userID is not valid. It must be a whole number.'
            });
            res.end();
        } else {
            // if there is not an input error
            // connect to teh database
            var connection;
            connectionPool.getConnection().then(function (con) {
                connection = con;
                // start a database transaction in order to assure consistent information
                return connection.beginTransaction();
            }).then(function () {
                // query the database in order to check the user has the required permissions to execute this method
                return connection.query('SELECT COUNT(*) AS authorisedCount ' +
                    '                   FROM Authorised_Users ' +
                    '                   WHERE Monitored_UserID = ? ' +
                    '                           AND Authorised_UserID = ? ' +
                    '                           AND Expiry_Time IS NOT NULL ' +
                    '                           AND Expiry_Time >= ? ' +
                    "                           AND DataTable = 'G'",
                    [actionOn, userID, new Date()]);
            }).then(function (authorisationResults) {
                if (authorisationResults[0].authorisedCount > 1 || userID == actionOn) {
                    //if the user is authorised, they can run it on them selves in which case the value from the database will be 0
                    // the user has been authorised
                    // read all of the gps readings which have not been analysed.
                    return readGpsReadings(connection, actionOn).then(function (gpsReadings) {
                        // read all of the geozones for an the user which is being analysed
                        return readGeozones(connection, actionOn, true).then(function (geozones) {
                            var currentZone = -1;
                            // error checking initialisation value
                            // a list of journeys where the entry is stored as a 3 element array of geozone primary keys and the time in which the person arrives at the end destination
                            var zoneConections = [];
                            // loop over all of the gps readings which have not been analysed
                            for (var counter = 0; counter < gpsReadings.length; counter++) {
                                // get the current iterations reading into a local variable
                                var reading = gpsReadings[counter];
                                // loop over the geo-zones and see if the coordinate is inside the zone
                                for (var innerCounter = 0; innerCounter < geozones.length; innerCounter++) {
                                    // store the current geozone in a local variable
                                    var zone = geozones[innerCounter];
                                    // check to see if the coordinate is in the current geozone by seeing if it is within the bounding box
                                    if (reading.Latitude >= zone.SouthMostLatitude &&
                                        reading.Latitude <= zone.NorthMostLatitude &&
                                        reading.Longitude >= zone.WestMostLongitude &&
                                        reading.Longitude <= zone.EastMostLongitude) {
                                        // if the current zone is not the initial state then add a journey to the list
                                        if (currentZone != -1 && currentZone != zone.GeozoneID) {
                                            zoneConections.push([currentZone, zone.GeozoneID, reading.Stamp]);
                                        }
                                        // update the current geozone
                                        currentZone = zone.GeozoneID;
                                        //continue; // a point should not be in two zones at once so there is no point continuing this loop as it could case bugs
                                    }
                                }
                            }
                            // after the journeys have been identified add them to the database
                            return Promise.map(zoneConections, function (zoneConnection) {
                                return connection.query('CALL Add_Journey(?,?,?,?)',
                                    [actionOn, zoneConnection[0], zoneConnection[1], zoneConnection[2]]);
                            });
                        });
                    }).then(function (someData) { // now look for odd behaviour in the visit data
                        // get a list of all the journeys made by a user historically
                        return connection.query('SELECT GPS_EndpointsID ' +
                            'FROM GPS_Endpoints ' +
                            'WHERE UserID = ?',
                            [actionOn]);
                    }).then(function (gpsEndpoints) {
                        // then for each journey
                        return Promise.map(gpsEndpoints, function (endpoint) {
                            // these variables are used to identify if the activity is always done on the same day
                            var daysOfWeek = -1;
                            var alwaysSameDay = true;
                            // get all the historic data in the journey occurrence table for the current journey
                            return connection.query('SELECT Stamp ' +
                                'FROM GPS_Endpoint_Occurance ' +
                                'WHERE GPS_EndpointsID = ? ' +
                                'AND Analysed = TRUE ',
                                [endpoint.GPS_EndpointsID]).then(function (analysedGpsEndpoints) {
                                // if there is at least one result set up the initial state of the variables
                                if (analysedGpsEndpoints.length >= 1) {
                                    // set the day of the week variable
                                    daysOfWeek = analysedGpsEndpoints[0].Stamp.getDay();
                                }
                                // loop over all of the gps endpoint occurrences
                                for (var count = 1; count < analysedGpsEndpoints.length; count++) {
                                    // get the current iteration day of the week
                                    var day = analysedGpsEndpoints[count].Stamp.getDay();
                                    //if the current day of the week is different from the first day of the week
                                    if (day != daysOfWeek) {
                                        // if this if statement is entered then the user does not do this on a regular basis
                                        alwaysSameDay = false;
                                        // break out of the loop because there is no point analysing the rest of the data as a answer is now known
                                        break;
                                    }
                                }
                            }).then(function () {
                                // if the user always does this journey onn the same day
                                if (alwaysSameDay) {
                                    // select all the journey occurences which have not been analysed.
                                    return connection.query('SELECT Stamp ' +
                                        'FROM GPS_Endpoint_Occurance ' +
                                        'WHERE GPS_EndpointsID = ? ' +
                                        'AND Analysed = FALSE ',
                                        [endpoint.GPS_EndpointsID]
                                    ).then(function (unAnalysedGpsEndpointOccurances) {
                                        // loop over all of the database rows and log any odd behaviour
                                        return Promise.map(unAnalysedGpsEndpointOccurances, function (unAnalysedOccurrence) {
                                            // if the day of the week does not match the historic pattern then raise an error
                                            if (unAnalysedOccurrence.Stamp.getDay() != daysOfWeek) {
                                                return addWarning(connection, 'Unusual Behaviour',
                                                    'Individual usually goes to the same location on a '
                                                    + dayOfWeekToString(daysOfWeek) +
                                                    ', however, they went on a ' +
                                                    dayOfWeekToString(unAnalysedOccurrence.Stamp.getDay())
                                                    + ' instead at ' + unAnalysedOccurrence.Stamp +
                                                    '.', 'GPS', actionOn);
                                            }
                                        });
                                    });
                                }
                            });
                        });
                    }).then(function (someData) {
                        // if the code reaches this point then everything has been successful so commit the changes
                        return connection.commit();
                    }).then(function () {
                        // then release the connection and then return a success message to the user
                        connection.release();
                        res.status(200);
                        res.json({
                            statusCode: 200,
                            Message: 'Analysis completed successfully!'
                        });
                    });
                } else {
                    // if unauthorised then rollback and respond with an unauthorised message
                    return connection.rollback().then(function () {
                        connection.release();
                        res.status(401);
                        res.json({
                            statusCode: 401,
                            Message: 'Unauthorised!'
                        });
                        res.end();
                    });
                }
            }).catch(function (error) {
                // catch all unexpected errors and respond them to the user
                connection.rollback().then(function () {
                    connection.release(); // release the connection
                    res.status(500);
                    res.json({
                        statusCode: 500,
                        Message: 'An unexpected server error has occurred.'
                    });
                    res.end();
                });
            });
        }
    }
});

// function for converting the days of the week as an integer to a string value
var dayOfWeekToString = function (dayOfWeek) {
    /*
    23/01/2018
     *
     * https://www.w3schools.com/jsref/jsref_getday.asp
    *
    * Used in order to create this function
     */
    // specify a list containing all the days of the week in the order such that the specified value corresponds to the index in the array
    var daysOfWeekAsStrings = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    if (dayOfWeek >= 0 && dayOfWeek <= 6) {
        return daysOfWeekAsStrings[dayOfWeek];
    }
    // otherwise parameter is invalid so do not return anything
}
// function for reading in all the system generated geozones associated with the user.
var readGeozones = function (connection, userID, isSystemGenerated) {
    return connection.query('' +
        'SELECT GeozoneID, Zone_Name, WestMostLongitude, EastMostLongitude, NorthMostLatitude, SouthMostLatitude ' +
        'FROM Geozone ' +
        'WHERE UserID = ? ' +
        '   AND IsSystemGenerated = ?',
        [userID, isSystemGenerated]);
}
// function for adding warnings to the database
var addWarning = function (connection, title, notification, notificationType, userID) {
    return connection.query('' +
        'INSERT INTO Notification (Title,Notification,Notification_Type,UserID) ' +
        'VALUES (?,?,?,?)',
        [title, notification, notificationType, userID]);
}

// A function for generating/regenerating the models used for the GPS analysis
router.get('/Build-GPS-Model/:UserID', function (req, res, next) {
    var token = checkToken(req, res);
    if (token) {
        // check to make sure
        var userID = token.UserID;
        // get the user id from the url
        var actionOn = req.params.UserID;
        // check the user id is invalid
        if (!Number.isInteger(parseInt(actionOn))) {
            res.status(400);
            res.json({
                statusCode: 400,
                Message: 'Invalid user id in the URL.'
            });
            res.end();
        } else {
            // if the input is valid then start a database connection
            var connection;
            connectionPool.getConnection().then(function (con) {
                connection = con;
                return connection.beginTransaction();
                // start transaction
            }).then(function () {
                // call function to get the list of permission the user has
                return getPermissionList(connection, userID, actionOn);
            }).then(function (permissionSet) {
                // delete all of the current system generated geozones as they need refreshing
                var hasPermissions = false;
                // loop over all of the permissions the user has
                for (var counter = 0; counter < permissionSet.length; counter++) {
                    if (permissionSet[counter].DataTable == 'G') {
                        hasPermissions = true;
                        // if the user has a record containing te GPS permission then set the permission variable to true
                        // the user is authorised so there is no point looking at the other rows
                        break;
                    }
                }
                if (actionOn == userID) {
                    hasPermissions = true;
                }
                // check to see if the user has the permissions
                if (!hasPermissions) {
                    // if unauthorised
                    res.status(401);
                    res.json({
                        statusCode: 401,
                        Message: 'Unauthorised!'
                    });
                    res.end();
                } else { // if the user is authorised
                    var finalCoordinates;
                    // delete all the current system generated zones
                    return connection.query('CALL Delete_System_Generated_Geozones(?)',
                        [actionOn]
                    ).then(function (someData) {
                        // select all of the gps readings for the user being acted upon
                        return connection.query('' +
                            'SELECT c.Latitude, c.Longitude ' +
                            'FROM GPS_Readings AS g ' +
                            'INNER JOIN Coordinate AS c ON g.CoordinateID = c.CoordinateID ' +
                            'WHERE UserID = ?', [actionOn]);
                    }).then(function (coordinates) {
                        // converting the format so that it matches that used by the GPS clustering library (list of lists)
                        return Promise.map(coordinates, function (dataItem) {
                            return [dataItem.Latitude, dataItem.Longitude];
                        });

                    }).then(function (adjustedCoordinates) {  // construct the data clusters
                        /*20/01/2017
                        *
                        * https://www.npmjs.com/package/dbscan_gps
                        *
                        *
                        *
                        * */    //get the library to perform clustering on the coordinates
                        finalCoordinates = adjustedCoordinates;
                        var clustering = new cluster(adjustedCoordinates, 0.02, "km", 30);
                        // convert the library to use promises. Unfortunately this can only be done on constructed objects rather than the whole library because it does not seem to be exposing the object
                        Promise.promisifyAll(clustering);
                        // perform the cluster analysis on the GPS coordinates. using the function generated by the promise library conversion
                        return clustering.fitAsync();
                    }).then(function (clusters) { // using all of the generated clusters
                        // for each generated cluster
                        return Promise.map(clusters, function (clusterItem) {
                            /*20/01/2018
                            * http://www.learner.org/jnorth/images/graphics/mclass/Lat_Long.gif
                            * Used to graphically understand which direction the GPS values correspond to
                            * */
                            var mostLeft = 180; // -180 is the most left so the opposite value is the starting value
                            var mostRight = -180; // 180 is the point most right so the complete opposite end of the spectrum is picked
                            var mostTop = -90; // 90 is the top of the earth so starting at the other end makes sense
                            var mostBottom = 90; // -90 is the furthest point south. so the opposite of 90 has been used
                            // loop over all the indexes held in the current cluster (list of coordinates in the cluster) such that a bounding box for the cluster can be created
                            for (var x = 0; x < clusterItem.length; x++) {
                                // get the current coordinate and place it in a local variable
                                var coordinate = finalCoordinates[x];
                                // coordinate[0] = latitude = North /South
                                // coordinate[1] = longitude = East / West
                                //latitude
                                if (coordinate[0] > mostTop) {
                                    mostTop = coordinate[0];
                                }
                                if (coordinate[0] < mostBottom) {
                                    mostBottom = coordinate[0];
                                }
                                // longitude
                                if (coordinate[1] > mostRight) {
                                    mostRight = coordinate[1];
                                }
                                if (coordinate[1] < mostLeft) {
                                    mostLeft = coordinate[1];
                                }
                            }
                            // end of for loop now add the cluster bounding box to the database
                            return connection.query('INSERT INTO Geozone (UserID, Zone_Name , IsSystemGenerated, WestMostLongitude, EastMostLongitude, NorthMostLatitude, SouthMostLatitude) ' +
                                'VALUES (?,?,TRUE,?,?,?,?);',
                                [actionOn, '', mostLeft, mostRight, mostTop, mostBottom]);
                        });
                    }).then(function () {
                        // the transaction has been successful so commit
                        return connection.commit();
                    }).then(function () {
                        // return success to the caller
                        res.status(200);
                        res.json({
                            statusCode: 200,
                            Message: 'Successfuly created / regenerated the analytics models.'
                        });
                        res.end();
                    });
                }
            }).catch(function (error) {
                // catch all of the unexpected errors and return it to the error
                connection.rollback().then(function () {
                    res.status(500);
                    res.json({
                        statusCode: 500,
                        Message: 'An unexpected internal server error has occurred.'
                    });
                    res.end();
                });
            });
        }
    }
});


// return all of the requests made on someone else or that have been made on you as well as all the access requests
router.get('/My-Requests', function (req, res, next) {
    var token = checkToken(req,res);
    if (token) {
        // check the user has provided a valid access token
        var userID = token.UserID;
        var accessRequests;
        var ruleRequests;
        // start communicating with the database
        var connection;
        connectionPool.getConnection().then(function (con) {
            connection = con;
            return connection.beginTransaction();
        }).then(function () {
            // select all of the rows which are valid which have the user as either being monitored
            // read rules cannot be used here as it  filters by the individual the rule is to be applied to
            return connection.query('' +
                'SELECT RuleID, Rule_Name, Action_Name, Action_On, Created_By, Authorised, DataTable ' +
                'FROM Rule r INNER ' +
                'JOIN Rule_Action ra ON r.Rule_ActionID = ra.Rule_ActionID ' +
                'WHERE (Created_By = ?  OR Action_On = ?) ' +
                '   AND (Expire_Stamp IS NULL  ' +
                '   OR Expire_Stamp < ?)',
                [userID, userID, new Date()]);
        }).then(function (rules) {
            // loop over all rules and add the parameters
            return Promise.map(rules, function (rule) {
                // read in all the parameters to the rule
                return connection.query('' +
                    'SELECT Parameter_Index, Parameter, Parameter_Type ' +
                    'FROM Rule_Parameter ' +
                    'WHERE RuleID = ?',
                    [rule.RuleID]
                ).then(function (ruleParameters) {
                    rule.Parameters = ruleParameters;
                    return rule;
                });
            });
        }).then(function (rules) {
            // using all of the fully read in rules
            ruleRequests = rules;
            return connection.query(
                'SELECT * ' +
                'FROM Authorised_Users ' +
                'WHERE (Authorised_UserID = ? OR Monitored_UserID = ?) ' +
                ' AND (Expiry_Time IS NULL  ' +
                ' OR Expiry_Time < ?)',
                [userID, userID, new Date()]);
        }).then(function (authorisationRequests) {
            accessRequests  = authorisationRequests;
            return connection.commit();
            // everything has been successful
        }).then(function () {
            // return success to the user
            connection.release();
            res.status(200);
            res.json({
                AccessRequests: accessRequests,
                RuleRequests: ruleRequests
            });
            res.end();
        }).catch(function (error) {
            // catch all encountered errors
            connection.rollback().then(function () {
                // rollback the database and return error to caller
                res.status(500);
                res.json({
                    statusCode: 500,
                    Message: 'An internal server error has occurred!'
                });
                res.end();
            });
        });
    }
});

// method for returning all warnings about an individual
router.get('/Warnings/:ActionOn', function (req, res, next) {
    var token = checkToken(req, res);
    if (token) { // valid token received
        var userID = token.UserID;
        var actionOn = req.params.ActionOn;
        if (!Number.isInteger(parseInt(actionOn))) {
            res.status(400);
            res.json({
                statusCode: 400,
                Message: 'Bad Request! The user id in the url is not an integer'
            });
            res.end();
        } else {
            // start communicating with the database
            var connection;
            connectionPool.getConnection().then(function (con) {
                connection = con;
                return connection.beginTransaction();
            }).then(function () {
                // get the list of permissions this user has the requested user
                return getPermissionList(connection, userID, actionOn);
            }).then(function (permissionList) {
                // main query to be used
                var query = 'SELECT * ' +
                    'FROM  Notification ' +
                    'WHERE UserID = ? ' +
                    '   AND Notification_Type IN (';
                // if there are no values
                if (permissionList.length == 0) {
                    // replace the query if the user has no permissions
                    query = 'SELECT * ' +
                        '   FROM  Notification ' +
                        '   WHERE TRUE = FALSE';
                } else {
                    query += "'" + permissionList[0].DataTable + "'";
                    // no escaping is used because it is from a system generated part of the database
                    for (var counter = 1; counter < permissionList.length; counter++) {
                        var permissionObj = permissionList[counter];
                        // no escaping has been used here because the value has come from the database
                        query += ",'" + permissionObj.DataTable + "'";
                    }
                    query += ')';
                }
                // execute the constructed query
                return connection.query(query, [actionOn]);
            }).then(function (notifications) {
                // for all the notifications log in the database that they have been sent to a the individual
                return Promise.map(notifications, function (notification) {
                    return connection.query('INSERT INTO Sent_Notifications (Sent_At,NotificationID,UserID) VALUES (?,?,?)',
                        [new Date(), notification.NotificationID, userID]);
                }).then(function (insertedRows) {
                    // if this point has been reached then commit and notify the caller of a successful operation
                    return connection.commit().then(function () {
                        connection.release();
                        res.status(200);
                        res.json(notifications);
                        res.end();
                    });
                });
            }).catch(function (error) {
                // catch all  unexpected errors
                connection.rollback();
                res.status(500);
                res.json({
                    statusCode: 500,
                    Message: 'An internal server error has occurred.'
                });
                res.end();
            });
        }
    }
    // otherwise there is an error reported in the check token method
});

// method to return the list of permissions a user has
var getPermissionList = function (connection, userID, actionOn) {
    return connection.query('' +
        'SELECT * ' +
        'FROM Authorised_Users ' +
        'WHERE Expiry_Time IS NOT NULL ' +
        '   AND Expiry_Time >= ? ' +
        '   AND Monitored_UserID = ? ' +
        '   AND Authorised_UserID = ?',
        [new Date(), actionOn, userID]);
}

// return a list of permissions which a user has on a monitored individual
router.get('/Connection/:MonitoredBy/Permissions', function (req, res, next) {
    var token = checkToken(req, res);
    if (token) { // valid token received
        var userID = token.UserID;
        var monitoredBy = req.params.MonitoredBy;
        if (!Number.isInteger(parseInt(monitoredBy))) {
            res.status(400);
            res.json({
                statusCode: 400,
                Message: 'The user id specified in the URL is invalid.'
            });
            res.end();
        } else {
            var connection;
            connectionPool.getConnection().then(function (con) {
                connection = con;
                return getPermissionList(connection, monitoredBy, userID);
            }).then(function (permissionList) {
                // return the list
                res.status(200);
                res.json(permissionList);
                res.end();
            }).catch(function (error) {
                // catch any unexpected errors
                res.status(500);
                res.json({
                    statusCode: 500,
                    Message: 'An internal server error has occurred.'
                });
                res.end();
            });
        }
    }
    // otherwise there is an error reported in the check token method
});

// method to remove a permission of a user
router.delete('/Connection/:MonitoredBy/Permissions/:PermissionID', function (req, res, next) {
    var token = checkToken(req, res);
    if (token) {
        // valid token received
        var userID = token.UserID;
        var monitoredBy = req.params.MonitoredBy;
        var permissionID = req.params.PermissionID;
        // validation
        var errorList = [];
        if (!Number.isInteger(parseInt(monitoredBy))) {
            errorList.push('The user id provided in the url is not an integer');
        }
        if (!Number.isInteger(parseInt(permissionID))) {
            errorList.push('The specified permission id provided in the url is not an integer');
        }
        // check to see if there have been any errors
        if (errorList.length > 0) {
            res.status(400); // report errors to the caller
            res.json({
                statusCode: 400,
                Message: 'Invalid request!',
                Errors: errorList
            });
            res.end();
        } else {
            // if there have been no errors
            // start connection to the database
            var connection;
            connectionPool.getConnection().then(function (con) {
                connection = con;
                // there is no need to check permissions as the filter will narrow the links between the two users and the connected user has been authorised with the token.
                return connection.query('' +
                    'DELETE FROM Authorised_Users ' +
                    'WHERE Authorised_UsersID = ? ' +
                    'AND Monitored_UserID = ? ' +
                    'AND Authorised_UserID = ?',
                    [permissionID, userID, monitoredBy]);
            }).then(function (result) {
                // return success
                res.status(200);
                res.json({
                    statusCode: 200,
                    Message: 'The authorisation has been revoked.'
                });
                res.end();
            }).catch(function (error) {
                // catch any unexpected errors
                res.status(500);
                res.json({
                    statusCode: 500,
                    Message: 'An internal server error has occurred.'
                });
                res.end();
            });
        }
    }// otherwise there is an error reported in the check token method
});

// reject a rule request
router.delete('/Reject-Rule/:RuleID', function (req, res, next) {
    var token = checkToken(req, res);
    if (token) { // valid token received
        var userID = token.UserID;
        var ruleID = req.params.RuleID;
        // validation
        if (!Number.isInteger(parseInt(ruleID))) {
            res.status(400);
            res.json({
                statusCode: 400,
                Message: 'Invalid Request! The rule id in the URL is not an integer'
            });
            res.end();
        } else { // if everything is valid connect to the database
            var connection;
            connectionPool.getConnection().then(function (con) {
                connection = con;
                // there is no need to check permissions as the filter will narrow the links between the two users and the connected user has been authorised with the token.
                return connection.query('CALL delete_rule(?,?)', [userID, ruleID]);
            }).then(function (result) { // report success
                res.status(200);
                res.json({
                    statusCode: 200,
                    Message: 'The rule has been revoked.'
                });
                res.end();
            }).catch(function (error) { // catch all errors
                res.status(500);
                res.json({
                    statusCode: 500,
                    Message: 'An internal server error has occurred.'
                });
                res.end();
            });
        }
    }
    // otherwise there is an error reported in the check token method
});

// reject (remove) access requests
router.delete('/Reject-Access-Request/:RequestID', function (req, res, next) {
    var token = checkToken(req, res);
    if (token) {
        // valid token received
        var userID = token.UserID;
        var requestID = req.params.RequestID;
        //validation
        if (!Number.isInteger(parseInt(requestID))) {
            res.status(400);
            res.json({
                statusID: 400,
                Message: 'Bad Request! The request id is not an integer.'
            });
            res.end();
        } else {
            // connect to the database
            var connection;
            connectionPool.getConnection().then(function (con) {
                connection = con;
                // there is no need to check permissions as the filter will narrow the links between the two users and the connected user has been authorised with the token.
                return connection.query('' +
                    'DELETE FROM Authorised_Users ' +
                    'WHERE Monitored_UserID = ? ' +
                    'AND Authorised_UsersID = ? ',
                    [userID, requestID]);
            }).then(function (result) {
                // return success message to the user
                res.status(200);
                res.json({
                    statusCode: 200,
                    Message: 'The rule has been revoked.'
                });
                res.end();
            }).catch(function (error) {
                // catch all unexpected errors
                res.status(500);
                res.json({
                    statusCode: 500,
                    Message: 'An internal server error has occurred.'
                });
                res.end();
            });
        }
    }
    // otherwise there is an error reported in the check token method
});

router.delete('/Delete-Account', function (req, res, next) {
    var token = checkToken(req, res);
    if (token) {// if authorised
        var connection;
        connectionPool.getConnection().then(function (con) {
            connection = con;
            con.query('CALL Delete_Account(?)', [token.UserID]);
        }).then(function (value) {
            connection.release();
            res.json({
                statusCode: 200,
                Message: 'Account deleted successfuly.'
            });
            res.end();
        }).catch(function (error) {
            connection.release();
            res.json({
                statusCode: 500,
                Message: 'Unable to delete account.'
            });
            res.end();
        });
    }
    // otherwise an error is reported
});



module.exports = router;
